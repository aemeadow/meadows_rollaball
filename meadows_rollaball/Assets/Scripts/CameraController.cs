﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    public Vector2 input;

    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = player.transform.position + offset;

        //this is the tilting the camera code

        Vector3 currentrot = transform.rotation.eulerAngles; 
        currentrot = new Vector3 (Mathf.MoveTowardsAngle (currentrot.x, input.y * -10f + 45f,Time.deltaTime * 10), 0f, Mathf.MoveTowardsAngle (currentrot.z, input.x * 10f, Time.deltaTime * 10));
        transform.rotation = Quaternion.Euler (currentrot);
        
    }
}
